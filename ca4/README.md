# Class Assignment 4 Report

#### 1. Producing ca3part2 with Docker instead of Vagrant:

- Installed dockertoolbox
- Started the Docker Quickstart Terminal


    docker info => OK
   
Then downloaded the repo: https://bitbucket.org/atb/docker-compose-spring-tut-demo/ into ca4

Changed in web\Dockerfile:

    RUN git clone https://mafaldasousa@bitbucket.org/mafaldasousa/devops-19-20-b-1191774.git
    
    WORKDIR /tmp/build/devops-19-20-b-1191774/ca2/ca2Parte2

And adapted, to my folder demo instead of basic:

    RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

#### 2. Use docker-compose to produce 2 services/containers (web/db):

Then in Docker Quickstart Terminal, went to my folder ca4, 
because it's the folder I have the Dockerfile and the docker-compose.yml, I put:

     docker-compose build 
 
Because I had a problem with gradlew, I changed again the web\Dockerfile, and added: 
    
    RUN chmod u+x gradlew

After this, went again to Docker Quickstart Terminal: 

    docker-compose build 
    
    docker-compose up
    
    docker-machine ip default: 192.168.99.101
    
In my browser I put: 

To access the database:     
*192.168.99.101/8082*

Then the login appeared and I put for the connection string use: 
*jdbc:h2:tcp://192.168.33.11:9092/./jpadb* and entered in the db.

To access the web application I put:
*http://192.168.99.101:8080/demo-0.0.1-SNAPSHOT/*

And saw the table with the employee data.

#### 3. Docker Hub: 

Gone to https://hub.docker.com and resgistered 
Then created a repo: devops_b_1191774_ca4

*https://hub.docker.com/repository/docker/1191774/devops_b_1191774_ca4*

In Docker Quickstart Terminal:

    docker longin
    
    docker ps -a : to see the namo of the images
    
    docker tag ca4_web 1191774/devops_b_1191774_ca4:ca4_web
    
    docker push 1191774/devops_b_1191774_ca4:ca4_web
    
    docker tag ca4_db 1191774/devops_b_1191774_ca4:ca4_db
        
    docker push 1191774/devops_b_1191774_ca4:ca4_db
    
#### 4.Copy db:

On terminal, in ca4 folder, executed:

    docker-compose up
     
    docker-compose ps -a
    
    docker-compose exec db /bin/bash 
    (if this doesn't wor, you can try docker-compose exec -it [container db number] /bin/bash)
    
    ls -al -> See the readme.txt
    
    cp jpadb.mv.db /usr/src/data
     
    ls -al -> See the readme.txt and the jpadb.mv.db
    
In this process I had a problem, because my folder was at /C: level and my docker at /C:/User level.
What happened? Well, the VM and my explorer weren't synchronized and I had to change my folder,
from /C: to /C:/User/MafaldaSousa to get a copy from jpadb.mv.db.

##### 6. Tag ca4:

    git add .
    
    git commit -m "Fix #16: CA4 - Docker related files" 
    
    git push
    
    git tag ca4
    
    git push origin ca4
---------------------------
#Kubernetes

#### 1. Compares Kubernetes to Docker: 

*Be aware that Kubernetes can be an alternative or a complement to Docker.*

*What they share:*

* Promisse to develope software one time and execute it everywhere.
* Microservice based architecture
* Open source 
* Lightweight
* YAML files to specify application stacks and their deployments

*What they differ:*

The great difference between Kubernetes and Docker it's that the first one execute in a cluster, coordinating it's nodes 
in an efficient way and the second execute into a single node. 

This is the reason why you should use them as a complement of each other, instead to choose only one.
 
The Kubernetes can orchestrate and manage all of the containers resources in a single plan and they are useful in network, 
balancing the charge, safety and sizing of all the nodes that the containers execute.

It has an isolation mechanism that allow to cluster the containers resources by access permission, test environment and others.
This make more easy and safe to share information to others developers or employees, allowing a collaboration between the teams 
without having to create a prototype of the entire application in the development environment. 

The Docker it's good for packaging and distributing the application in containers, that you can create or execute, as well as store or share container images.

However, if the app starts to receive more information and need to increase, you can create more containers and nodes to the Cluster with Kubernetes.

You may use the two to make a structure more strong and increase the app availability.
The App will be Online even if some part of the team it's off.

Sometimes, when you see some comparisson to the Kubernetes vs Docker, you will see some vs to Docker Swarm.
This happens because Docker Swarm can cluster the docker containers and uses it's own API.

However, even I recommend the use of the two, if you want to escalate you application.

#### 2. Describe how the alternative tool could be used to solve the same goals:

Here we can use kubernetes as a complement with docker. 

So you need to instal kubernetes, in my case, because I used Docker Toolbox, I have to instal Minikube.

After that I have to start the Minikube: 

    minikube start
    
    minikube status
    
    minikube docker-env

In this poins I had a problem to point my shell to minikube's docker-daemon, it suggests to run:

    @FOR /f "tokens=*" %i IN ('minikube -p minikube docker-env') DO @%i 
   
If everything was ok, I would execute the following commands to run the app and create the containers (web/db):

    docker-compose build
    
    docker-compose up 

Then, in my browser, to access the database:     
*192.168.99.101/8082*

To access the web application I put:
*http://192.168.99.101:8080/demo-0.0.1-SNAPSHOT/*


However, I have a network configuration problem that I cannot fix.
Discovered it when I ran:

    minikube logs

------------------------
# Docker Compose VM Demonstration - Professor demonstration

This project demonstrates how to run a container with Tomcat and other with H2.

## Requirements

Install Docker in you computer.

## How to build and execute

In the folder of the docker-compose.yml execute

  ```docker-compose build```

The execute

  ```docker-compose up```

### How to use

  In the host you can open the spring web application using the following url:

  - http://localhost:8080/basic-0.0.1-SNAPSHOT/

  You can also open the H2 console using the following url:

  - http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console

  For the connection string use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb  