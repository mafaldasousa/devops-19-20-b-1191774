# Class Assignment 1 Report
_______________________________
https://bitbucket.org/mafaldasousa/devops-19-20-b-1191774/src/master/
____________________

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

-----------------------------------------

To complete this assigment I previous had: Version Control: Git / Repository: Bitbucket / IDE: IntelliJ IDEA

After connected to the repository, create *.gitignore* and change the *parent* in *pom.xml*

`git init`
`git commit -m "Fix #1 Initial commit"`
`git push`

#### 1.1 In comand line I start to assure that I was in the master branch:

`git status`

#### 1.2 Tag the current branch with a version:

`git tag -a v1.2.0 -m "my version 1.2.0"`

`git push origin v1.2.0`

#### 1.3 Add a new email field to the app in branch email-field: 

##### 1.3.1 Create a new branch to work:  

`git branch email-field`

`git checkout email-field`

`git status (can use this to assure that you are working on branch email-field)`


##### 1.3.2 Add email field in files:

*- Employee.java* 

*- DatabaseLoader.java*  

*- app.js*


##### 1.3.3 Add tests to email field:

*create a path to src/test/java/com/greglturnquist/payroll in intellij*

`cd src/test/java/com/greglturnquist/payroll`

`echo > EmployeeTest.java`

`git add EmployeeTest.java`

*add unit tests to emailField: HappyPath, Null, Empty*

`git commit -m "#2: CA1 - add emailField and Test"`

`git push`

##### 1.3.4 Debug in intelij 

*Tests: ok*

##### 1.3.5 Merge email-field branch with the master

`git checkout master`

`git merge email-field`

`git push`

`git tag -a v1.3.0 -m "my version 1.3.0"`

`git push origin v1.3.0`


#### 1.4 Fixing bugs in branch ﬁx-invalid-email: to Employee accept only valid emails 

##### 1.4.1 Creating a branch called ﬁx-invalid-email 

`git branch ﬁx-invalid-email`

`git checkout ﬁx-invalid-email`

`git status`

*Files corrected:* 
*- Employee.java* 
*- EmployeeTest.java* 

`git commit -m "Fix #3: CA1 - Add email parameters to Classes Employee and EmployeeTest"`

`git push --set-upstream origin ﬁx-invalid-email`

##### 1.4.2 Debug in intellij

*Tests : ok*

To assure your changes are ok you can run:

`/mvnw spring-boot:run`

and visit *http://localhost:8080*

##### 1.4.3 Merge into master branch

`git checkout master`

`git merge fix-invalid-email`

`git tag -a v1.3.1 -m "my version 1.3.1"`

`git push origin v1.3.1`

#### 1.5 Add Tag ca1

`git tag -a ca1 -m "exercise ca1: git"`

`git push origin ca1`

## Deleted old branches

*In this case we could let the old branches, however, there are two main reasons to delete old branches:*

*- They are no longer needed and,*

*- With the development of the project, it's more difficult to work with a list of old branches.*

`git branch -d email-field`

`git branch -d ﬁx-invalid-email`



## 2. Analysis of an Alternative
------
### Git vs Mercurial

*For this assignment is there an aterntiave tool for Git? Describe it and compare it to Git*

Created at the same time, in April 2005, to replace another VCS, Bitkeeper, however we know that about 80% of the current market uses Git and only 2% uses Mercurial.

Both are a form of VCS with the purpose of helping developers and/or their teams to manage the history of versions of the source code.

There is no consensus on which tool is better, Git is more complex because it is more flexible and a command has many options that easily change its functionality. 

Here we also have the possibility of reverting to older versions, and it may be possible to change the version history, 
for these reasons this tool is recommended for teams with more experience or more  technicians. 

Mercurial, on the other hand, presents a simpler language, and doesn’t let the story line of versions change so easily, so it is more suitable for junior and/or less technical teams.

The biggest differences between these two VCS are in the terms *Branch* and *Staging Area*.

The Branch concept has a different definition, in Git it works as references to certain commits, and we can create, delete and change the branches at any time, without affecting/changing the commits.

In Mercurial, it corresponds to a line of change sets, in which the branches, after the commit, cannot be removed, as this would change the version history. 

Besides that it is more difficult to work, as they advise you to clone the repository when creating each branch and you also need to be very careful because we can work on the wrong branch, 
especially if they are not well represented. 

An alternative in Mercurial, where we see a behavior similar to the branch in Git, is to use bookmarks, which reference the commit and then merge to the original/master bookmark.

The other concept is the Staging Area, also known as index, in which through `git add` we add all files with changes that we intend to associate with a commit, 
which allows a selection of the changed files that we intend to send to the repository. 

In Mercurial, there is no staging area, so all changes are always sent in the next commit.


## 3. Implementation of the Alternative
__________________

https://1191774isepipppt@helixteamhub.cloud/young-thunder-4946/projects/devops-19-20-b-1191774_ca1/repositories/mercurial/devops-b-1191774
____________________________

To complete this assigment I previous had: Version Control: Mercurial / Repository: HelixTeamHub / IDE: IntelliJ IDEA

After connected to the repository, create *.hgignore* and change *.pom*

`hg init`
`hg commit -m "#1 Initial commit"`
`hg push`

#### 3.1. In comand line I start to assure that I was in the master branch and what bookmark we use:

`hg identify -b` or `hg branch` => default (master)

`hg bookmarks` => no bookmarks set

*We must create a new bookmark - it's more similar to Git's branch*

`hg bookmark master`

#### 3.2.Tag the current bookmark with a version:

`hg tag -m "My version v1.2.0" v1.2.0`

#### 3.3 Add a new email field to the app in branch email-field: 

##### 3.3.1 Create a new branch to work: 

`hg bookmark email-field`

`hg bookmarks` 

*Here you will see wich bookmark has an *, the one with that it's the bookmark you are working on.*

##### 3.3.2 Add email field in files:

*- Employee.java* 

*- DatabaseLoader.java*  

*- app.js*


##### 3.3.3 Add tests to email field:

*create a path to src/test/java/com/greglturnquist/payroll in intellij*

`cd src/test/java/com/greglturnquist/payroll`

`echo > EmployeeTest.java`

`hg add EmployeeTest.java`

*add unit tests to emailField: HappyPath, Null, Empty*


##### 3.3.4 Debug in intelij 

*Tests: ok*

##### 3.3.5 Merge email-field branch with the master

`hg tag -m "My version v1.3.0" v1.3.0`

`hg commit -m "#2 add emailField"`

`hg update master`

`hg update`

`hg push`

#### 3.4 Fixing bugs in branch ﬁx-invalid-email: to Employee accept only valid emails 

##### 3.4.1 Creating a branch called ﬁx-invalid-email 

`hg bookmarks`

`hg bookmark ﬁx-invalid-email`

`hg bookmarks`

*Make Employee accept only valid emails, and test it.*

##### 3.4.2 Debug in intellij

*Tests : ok*

##### 3.4.3 Merge into master branch

`hg tag -m "My version v1.3.1" -r 1 v1.3.1`

`hg commit -m "#3: Add email parameters to Classes Employee and EmployeeTest"`

`hg update master`

`hg bookmark`

`hg update`

`hg push`

#### 3.5 Add Tag ca1

`hg tag -m "My exercise CA1"  ca1`

`hg push`

## 4. Assigment Corretion

This section is to clarify the Professor about one mistake I made. 
In implementation of Git, exercises 1.3.5 and 1.4.3, I hat to add an Tag, however, even I created the Tag, locally, 
I dind't do the necessary `git push origin v1.3.0` and `git push origin v1.3.1`. 

*However, I hope this can show you how I learned and grew with this assignment.*