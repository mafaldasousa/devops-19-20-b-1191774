# Class Assignment 3 Part 1 Report

**1. Creating a VM**

Everything went well, as the professor explained during the session, which made an example of the creation of the VM. 

However, when I was installing git, an error occur. To solve thins  constraint, before the command in professor's pdf, 
I executed the command:

    sudo apt update

After that, I executed:

    sudo apt install git

To verify that I installed git and that it was ok: 

    git --version 

That gave me the installed version of git, in this case 2.17.11, current version for Ubuntu 18.04. 

**2. Clone your individual repository inside the VM**

Executed the next command: 

    git clone https://mafaldasousa@bitbucket.org/mafaldasousa/devops-19-20-b-1191774.git

**3. Install dependencies**

I already have FileZilla, git, and jdk installed I installed maven, gradle, all with the command: 

    sudo apt install <name>

**4. Reply CA1**
 
Here I replied, in a much more simplified version the exercise of CA1, as the professor demonstrate in devops05.pdf. 
And verified, on the my browser, that the application was running ok: http://192.168.56.100:8080/   

**5. Reply CA2**
 
Here I replied, in a much more simplified version the exercise of CA2, as the professor said.  
First I had to change build.gradle, instead of 'localhost' I had to put my IP. 
Then, how I had cloned my repository, I entered in CA2Parte1 and execute: 

    ./build gradle 
    ./gradle run 

In 2 command lines of your laptop, execute this command and start to chat:

    gradlew runClient

*Because of VM don't have an graphic part, for that you have to install a desktop version, and you don't want it for now.
This is the reason why you have to access outside of VM to the chatbox, in our command line and create the different users.* 