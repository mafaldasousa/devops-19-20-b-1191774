# Class Assignment 3 Part 2 Report

**1. Vagrant Multi VM Demonstration for Spring Basic Tutorial Application**

Following the teacher's instructions at *devops06.pdf*:

- Installed VM and Vagrant 
- Then create a new directory to initialize a vagrant project
- Create a new Vagrant configuration ﬁle for the project and Start the VM  
- This created a .vangrantfile that later I copied and present here, in ca3parte2

**2. Configuration of CA2Parte2 and .vangrantfile**

Configured the files of CA2Parte2 and .vangrantfile so I can use the gradle in my project CA2Parte2 and my VM, doing exactly as the Professor have done.

*In the CA2Parte2, followed the professor repository instructions:*

Altered the following files:
- build.gradle - to support war file
- application.properties - to support h2 console/to remote access and to retain info, instead of drop it on every execution 
and add app context path
- app.js - to support app context path
- index.html - to correct a ref to css

Create the following file:
- src/main/java/com/greglturnquist/payroll/ServletInitializer.java - to support war file

*In the Vagrant, after some changes in my vagrant file:*

    vagrant reload --provision
    
This changes were:

- Update of my repository on git clone
- Changed the basic name folder to demo 
- Add chmod +x gradlew 
- Add sudo to ./gradlew clean build

Because I had some difficulties with permissions.

However, some of the same complications got in the way once again and others were new: 
1st I had to turn my Repository public
2nd I have trouble with my access permissions to the files in CA2Parte2, even after I changed the .vagrantfile, 
to solve that in command line: 

    cd c:/vagrant-project-1/vagrant
    
    vagrant ssh web
    
    ls
    
     rm -r -f node_modules/ 
     CA2PArte2 had some node files that were affecting the process so I had to remove it
      
     ls
     
     git status
     
     git add . 
     
     git config --global user.email "1191774@isep.ipp.pt"
     
     git config --global user.name "Mafalda Sousa"
     
     git commit -m "Limpeza de mais algumas pastas desnecessárias"
     
     git push
     
     rm -r -f devops-19-20-b-1191774/ 
     Decided to do that to clone a fresh new project
     
     git clone https://mafaldasousa@bitbucket.org/mafaldasousa/devops-19-20-b-1191774.git
     
     cd devops-19-20-b-1191774/ca2/ca2Parte2
     
     chmod +x gradlew
     
     sudo ./gradlew clean build

After this, in my browser I put http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/ 
and appeared all the employees in the db. 

_____________________

##Alternative

####1. Alternative tool to VirtualBox

A virtual machine allow you to run an OS inside other OS, that way, for example, you can run a Linux as a guest inside a windows OS.
This happen due to the abstraction layer of a special software hypervisor that makes VMs independent from the hardware.

There are 2 types of hypervisors: 1 is native and run in hardware directly, without an OS, and another type, that needs the OS to host.
The first type is more useful for large production environments and as example we have the VMware ESXi.
The second type is more convenient to run a VM on a personal computer and here we have VistualBox, VMware Workstation Pro, ...

The utility of the virtualization make it very common among the corporation, because they can save some money, time and space:

- You can run multiple applications in multiple OS at the same time in the same hardware
- You can use it as an alternative OS to remote workers, because it is easy copy machines from one host to another.
 
Although We must be aware that you will devide your memory resources the host OS and the guest OS, that might decrease the performance of some activities in PC.

Of course, that the most popular is VirtualBox, but there are a lot of alternatives:

- VMware Workstation (Pro/Player), 
- Qemu, 
- Parallels, 
- Windows Virtual PC, 
- KVM, 
- XenServer ...

To present here, I will choose the  VMware Workstation Pro.
The Pro is the paid version of this application, actually, it's the most expensive alternative to the VistualBox.
But has some advantages, otherwise it wouldn't be one of the best in virtualization, because it is stable 
and has multiple uses, even if you have a lot to config before use it, you can, for instances emulate mobile or tablets.

The VirtualBox and  VMware Workstation Pro are both fast, reliable, and include a wide range of interesting features. 
Some of these features are available in both platforms, but there are also some features that are unique to each platform. 
The choice between VMware and VirtualBox virtualization solutions may be difficult.

VirtualBox can be used with any OS but VMware Workstation Pro doesn't have support for MacOS.

VirtualBox has Guest Additions, this is a set of drivers and system applications for guest operating systems, 
which can be used to improve VM performance and usability. 
Guest Additions are provided for all supported guest OS, and the VBoxGuestAdditions.iso file 
is located in the directory where VirtualBox is installed. 

VMware Tools is the analog of VirtualBox Guest Additions for virtual machines that are running on the VMware platform. 
It is provided for each supported guest OS as a separate ISO image (windows.iso, linux.iso ...) 
and is located in the directory of the application. 
 
This will make available the following features: 
Shared folders, Drag & Drop, Shared Clipboard, Integrated mouse pointer, Autologon, better video output, time synchronization, etc.

The feature that allow to display windows of guest applications, like how windows of host applications are displayed 
are VirtualBox Seamless mode and VMware Unity.
VirtualBox Guest Additions and VMware Tools are required to make this possible. 

They both have Snapshots, this last allow to save a VM state for a particular moment in time.
Multiple snapshots can be created and then roll back a VM to one of the snapshots in order to restore the VM state.
When a snapshot of the running VM is taken, the virtual memory is also saved to a file, and this happens everytime 
you take a snapshot, but to many snapshots may decrease the performance of you VM. However you mustn't consider use this as a backup.

For a 3D graphic, VMware have a better and not so limited support as VirtualBox.
The opposite happens when we talk about Virtual Disk Format, that VirtualBox supports VDI, VMDK, VHD and HDD and the VMware only support VMDK.
We can see the same for Virtual Networks, VirtualBox supports Not attached, NAT, NAT Network, Bridged adapter, Internal network and Host-only adapter.
The VMware support Bridged, NAT, and Host only.

Other features that both have support: USB Devices, VM Encryption, Memory Ballooning and Linked clones.

Features that only VirtualBox have support: Shared storage, VM Live Migration and Centralized Management.

So, to choose between the two you need to balance your needs, specifications and, of course, your budget.

####2. How the alternative tool could be used with vagrant

First you have to download the VMware Workstation Pro, Vagrant and the vagrant plugin to your desktop.
The problem with this solution is that you need to buy the VMware Workstation Pro and the vagrant plugin.

Beside that, is necessary to adapt the vagrantfile to use the VMware,
but the things like vagrant up or vagrant ssh they will work the same. 
 
 After all programs installed, must be configured the settings for the provider in the Vagrantfile:
 In all the places in the vagrantfile with "envimation/ubuntu-xenial" we will change to "vmware_desktop"
 and adapt the initial parapher to:
 
     Vagrant.configure("2") do |config|
       config.vm.box = "my-box"
       config.vm.provider "vmware_desktop" do |v|
         v.gui = true
       end
     end
 
To add setting for memory you have to change:

     web.vm.provider "virtualbox" do |v|
       v.memory = 1024
     end
     
 For:
 
      config.vm.provider "vmware_desktop" do |v|
       v.vmx["memsize"] = "1024"
     end

The rest it's the same, I think, because you need to have the others settings for db, h2, war file, gradle.   

    

