package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EmployeeTest {

    @org.junit.jupiter.api.Test
    void setEmailFieldHappyPath() {
        //Arrange
        Employee employee = new Employee();

        String expectedResult = "user@finances.com";
        employee.setEmailField("user@finances.com");

        // Act
        String result = employee.getEmailField();

        // Assert
        assertEquals(result, expectedResult);
    }

    @Test
    void setEmailFieldNull() {
        //Arrange
        Employee employee = new Employee();

        //Arrange//Act//Assert
        Exception exception = assertThrows(NullPointerException.class, () -> {
            employee.setEmailField(null);
        });

        String expectedMessage = "e-mail cannot be null";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void setEmailFieldEmpty() {
        //Arrange
        Employee employee = new Employee();

        //Arrange//Act//Assert
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            employee.setEmailField("");
        });

        String expectedMessage = "e-mail is invalid";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void setEmailFieldWithoutAt() {
        //Arrange
        Employee employee = new Employee();

        //Arrange//Act//Assert
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            employee.setEmailField("user.finances.com");
        });

        String expectedMessage = "e-mail is invalid";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }


}
