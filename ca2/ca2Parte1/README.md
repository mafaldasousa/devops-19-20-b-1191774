# Class Assignment 2 Part 1 Report
====================

This is the report to the class assignment 2 - Part 1 

**1.** Go to repository and download the gradle_basic_demo: 
https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/
Then create a folder: ca2\ca2part1 and copy the gradle_basic_demo content into it.

Then install and make it operable, if you don't have it yet: _gradle_

##
**2.** Go to README.md in ca2\ca2part1, and follow the steps to create a chat between two clients, and "write" to each other to see if worked!
README.md: 

###### Gradle Basic Demo

This is a demo application that implements a basic multithreaded chat room server.

The server supports several simultaneous clients through multithreading. When a client connects the server requests a screen name, and keeps requesting a name until a unique one is received. After a client submits a unique name, the server acknowledges it. Then all messages from that client will be broadcast to all other clients that have submitted a unique screen name. A simple "chat protocol" is used for managing a user's registration/leaving and message broadcast.


###### Prerequisites

 * Java JDK 8
 * Apache Log4J 2
 * Gradle 6.6 (if you do not use the gradle wrapper in the project)
   

###### Build

To build a .jar file with the application:

    % ./gradlew build 

###### Run the server

Open a terminal and execute the following command from the project's root directory:

    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>

Substitute <server port> by a valid por number, e.g. 59001

###### Run a client

Open another terminal and execute the following gradle task from the project's root directory:

    % ./gradlew runClient`

The above task assumes the chat server's IP is "localhost" and its port is "59001". If you whish to use other parameters please edit the runClient task in the "build.gradle" file in the project's root directory.

To run several clients, you just need to open more terminals and repeat the invocation of the runClient gradle task

##
**3.** Add a new task to execute the server.

Add in _build.gradle_: 

    task runServer(type:JavaExec, dependsOn: classes){
        group = "DevOps"
        description = "getGreeting in a chat client that connects to a server on localhost:59001 "

        classpath = sourceSets.main.runtimeClasspath
    
        main = 'basic_demo.ChatServerApp'
    
        args '59001'
    }

command line: 
`gradlew runServer`

##
**4.** Add a simple unitTest

The unit tests require junit 4.12 to execute, so in build.gradle add:

_in dependencies: implementation 'junit:junit:4.12'_

in IntelliJ create a path 
`src\test\java\basic_demo\AppTest`

add code test (copy from professor power point)

command line: 
`gradlew test` 
  
To see if the test is running and then we can check in: 

    build\reports\tests\test: index.html and see in a browser how successful were the tests

##
**5.** Create a Copy task

in build.gradle create: 

    task runBackupSrc(type:Copy){
    from 'src'
    into 'backup'
    }

Then, in command line write: 
`gradlew runBackupSrc`

This show us if the task is running and, if it is, create a folder backup, with main and test copies.

This package creation just occur if the package doesn't exist, but if it exists, running this task will only do an update.

Then we add this folder to .gitignore, because is a backup file so shouldn't be in the repository.

However, if the Professor want to see that folder, please, feel free to ask.

##
**6.** Create a Zip task

In build.gradle create: 

    task runZip(type:Zip){
        archiveFileName = "mySrc.zip"
        destinationDirectory = file ("zipFile")

        from "src"
    }

Then, in command Line write: 
`gradlew runZip`

This show us if the task is running and, if it is, create a folder zipFile, with a file: mySrc.zip.

This package creation just occur if the package doesn't exist, but if it exists, running this task will only do an update.

##
**7.** To finnish this exercise, add a tag ca2-part1

in command line:

`git add . => to add all the files to the staging area`

`git status` _=> to confirm that and if we are working on master branch_

`git commit -m "fix #7: exercise CA2-PART1"`

`git push`

`git tag -a ca2-part1 -m "my exercise ca2-part1"` _=> to tag the exercise_

`git push origin ca2-part1 `

##

Update the README.md file

`git add . `

`git commit -m "fix #8 Update the README.md file"`

`git push`